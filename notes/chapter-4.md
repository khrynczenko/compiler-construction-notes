# Parsing (syntactic analysis) 

Parsing (the next step after scanning) works with tokens and constructs
sentences out of them. A formal way of describing parsers is using Context
Free Grammars (CFG). CFGs are much stronger than regular expressions (they
allow recursion) so they cab be used do describe more complex structures.
Grammars are easy to write but they are not necessary easy to parse. Writing
grammar requires a lot of attention so no ambiguities or other problematic
things are made. That is why two subsets of grammars are distinguished.

- LL(1) - left-to-right leftmost derivation
- LR(1) - left-to-right rightmost derivation

One in the parentheses means that only next token in the stream of tokens
needs to used to further advance parsing process. LL(1) grammars are less
powerful and not all languages can be described by it but it is simple to
implement using for example method known as *Recursive Descent Parser`.
LR(1) grammars are more general an powerful but implementing them takes
much more work and usually parser generators are used to build the parsers
for them.


## Context Free Grammar
Some terms:
- *terminal* is a symbol that can appear in the language. In our case its
  simply a token
- *non-terminal* represents a rule in the grammar and that rule build a
  sentence out of tokens
- *sentence* valid sequence of terminals in a language
- *sequential form* is a valid sequence of terminals and non-terminals

A *context-free grammar* is a list of rules that describe allowable
sentences in a language. Example of a rule would be:  
```
DECLARATION -> identifier_token equal_token EXPRESSION
EXPRESSION -> ...
```
Each grammar describes a (possibly infinite) set of sentences which is known
as a language of the grammar. A sequence of rule applications is known as
**derivation**.

Given a grammar:
```
P -> E
E -> E + E
E -> identifier
E -> int
```
And a program like:  
`identifier + int + int`  
Derivation can be done in two ways:  
Using `top-down derivation`:
```
(Sentential form | Apply rule)
P | P -> E
E | E -> E + E
E + E | E -> indentifier
identifier + E | E -> E + E
identifier + E + E | E -> int
identifier + int + E | E -> int
identifier + int + int |
```
Or using `bottom-up derivation`
```
(Sentential form | Apply rule)
identifier + int + int | E -> int
identifier + int + E | E -> int
identifier + E + E | E -> E -> E + E
identifier + E | E -> identifier
E + E | E -> E + E
E | P -> E
P
```
So in `top-down derivation` we begin with start non-terminal symbol and
advance (using rules) till we get a sentence built only from non terminals.
In `bottom-up derivation` we start with a sentence we want to build and then
apply the rules in a backward manner until we reach *start symbol*.

*Grammar is a finite set of rules.*  
*Language is a set of strings generate by grammar.*

If two different grammars generate same language then they have **weak
equivalence**.

## Ambiguous grammar
Ambiguous grammar allows to derive the sentence in more that one way.  
![](../static/ambiguous-grammar-1.png)  
Here it can be seen that two different parse-trees are derived for the
same sentence. I we would add multiplication to our grammar these would
mean we could have different order of calculation for the same mathematical
expression. Ambiguity like that represents a problem because it can give
a program two different possible meanings.

In order to solve ambiguity we should make our grammar accept only one type
of derivation (leftmost or rightmost). We can to that in many ways. For
example for rule for binary operators we could require one of the operands to
be an atomic term:
```
P -> E
E -> E + T
E -> T
T -> ident
T -> int
```
Now this grammar allows only left-most derivation (in fact it is LL(1)
grammar)  

## LL Grammars
LL(1) grammar is a grammar that can be parsed using only one non-terminal
and the next token in the input stream. To ensure this:
- Remove any ambiguity as shown previously
- Eliminate left recursion
- Eliminate any common left prefixes
After taking these steps one should generate **FIRST** and **FOLLOW** sets
for the grammar and with the create the parse table. If the parse table
has no conflicts we can be sure that we have LL(1) grammar.


### Eliminating Left Recursion

Left recursion is involved when grammar contains rule such as  
`A -> Aa`  
or more generally  
` A-> Bb` such that `B -> Ac` by some sequence of derivations.
We can fix that by replacing the recursive rules so they begin with the
leading symbols of its alternatives.
So having
```
A -> Aa | Ab | c
```
we would rewrite it to:
```
A -> cA`
A` -> aA` | bA` | (empty)
```


## Eliminating Common Left Prefixes
We look for all common prefixes for a given non-terminal and replace them
with a rule that contains such prefix and other that contain the variants.
From this:  
```
A -> ab | ac
```
To:
```
A -> aA`
A` -> b | c
```

## First and Follow Sets

**FIRST** set contains terminals that can appear at the beginning of any
derivation of a given non-terminal.  
**FOLLOW** set contains terminals that could potentially occur after any
derivation of a given non-terminal.
![](../static/first.png)  
![](../static/follow.png)  

For a grammar:
```
P -> E
E -> T E'
E' -> + T E'
T -> F T'
T' -> * F T'
T' -> (empty)
F -> (E)
F -> int
```
First:
```
FIRST(P) = {(, int}
FIRST(E) = {(, int}
FIRST(E') = {+, (empty)}
FIRST(T) = {(, int}
FIRST(T') = {*, (empty)}
FIRST(F) = {(, int}
```
Follow:
```
FOLLOW(P) = {$}
FOLLOW(E) = {), $}
FOLLOW(E') = {), $}
FOLLOW(T) = {+, ), $}
FOLLOW(T') = {+, ), $}
FOLLOW(F) = {*, +, ), $}
```

## Parsing LL(1) Grammar
There are two common ways of parsing LL(1) grammars. One is using
*recursive descent parsing* and other uses *table driven parsing*.

### Recursive descent parser
RDP basically forms a function for each non-terminal and in that function
advances on terminals (tokens) and calls other functions for non-terminals
until the input stream is parsed or it is impossible to advance further.
Example implementation would look like:
![](../static/recursive-descent-parsing.png)  


### Table Driven Parsing