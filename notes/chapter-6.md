# The Abstract Syntax Tree

*AST* is fundamental data structure that represents the primary structure
of the program. It is the starting point for semantic analysis of a program.
Abstract term comes from the fact that the details of parsing are omitted i.e.
it does not care whether language has pre/post or infix notation.

- *Declaration* states the name, type and value of a symbol so that it can
  be used in a program.
- *Statement* is an action that when carried out changes state of the program.
  For example loops, conditionals and function returns.
- *Expression* is a combination of values and operations that in the end yield
  a value (like integer, float, string etc.). In some language expression can
  also have a *side effect* that changes the state of the program.

Each of these should have a representation in the *AST*.  
For example declaration like
`s: string = "hello";`
or
`f: function integer(x: integer) = { return x * x; }
could be defined in *C* using following structure
```C
struct decl {
    char* name;
    struct type* type;
    struct expr* value;
    struct stmt* code; // this is for function decleration
    struct decl* next; // since b-minor program is a list of declaration
                       // decl can be already root for our AST
}
```
