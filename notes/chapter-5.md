# Parsing in Practice

This chapter mostly talks about using parser generators (bison in this case).
Since I am building my own parser I skip most of this chapter and only
highlight some interesting information.

- *Validator* is a program that reads an input program and simply verifies
whether it is acceptable and conforms with the grammar.
- *Interpreter* reads the input program and executes it while reading.
- *Translator* read the input program, parses it to abstract syntax tree and
then produces the same program in different language.

Performing computation directly while parsing is not a good idea. If there
would be an error at the end of the program we would waste a lot of resources
to just find that. It is much better to first parse program and produce
*abstract syntax tree* which can be then traversed to execute (or translate
for example).
