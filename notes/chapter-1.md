# Introduction (Chapter 1)

A *compiler* is a program that accepts some kind of source language and
translates it to target language. But that is not an entire truth. Compiler is
also meant to improve the program in this process. If all what happens is a
translation it would be a *transpiler* or *translator*. Examples of translators
are `pandoc` and other programs that convert `markdown` files to `html` or
other target representations.

In their work "Engineering a compiler" Torczon et al. state that compiler must
also improve the source program (language) in some desirable way. For example
it could check program for correctness, remove unnecessary parts or improve the
performance. Another condition for compilers is to preserve the meaning of the
original program. Exemplary compilers are `g++`, `clang` or `rustc`. These
programs take *C++* and *rust* programs and compile them into optimized machine
representations.

Closely related to compilers are interpreters. They omit the translation part
and execute the source program while reading it. Popular interpreter are
`Python Virtual Machine` which interprets *python* bytecode.

And with that it is necessary to make another distinction for *hybrid
interpreters* which perform both compilation and interpretation process.
Python (as a `python` program) is a hybrid interpreter. First it compiles
python source language into python bytecode which is then executed (interpreted)
by the internal program (PVM).  
In very similar way works java. Java code is translated to Java bytecode and
then processed by *JVM*. Yet there is another aspect to Java and it is
*just-in-time* (JIT) compilation. Basically JVM is capable of compiling parts
of the bytecode into machine code at runtime (interpretation step).
This technique is getting very popular within cotemporary hybrid interpreters.