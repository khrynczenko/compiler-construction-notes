# A quick tour (Chapter 2)
## Compiler does not work alone

*Compiler* is usually a part of a bigger toolchain. If we would
look at what happens when we compile *C* program such toolchain
would look like this.

*preprocessor* -> *compiler* -> *assembler* -> *linker*

On *Unix*-like system the counter part programs would be:

*cpp* -> *cc1* -> *as* -> *ld*

## Compiler is built of many components

### Scanner
Scanner goes through the source code and recognizes individual
tokens. From single characters it build stream of tokens.  

`var x = 1`

Output of the scanner for the line like that could look like:

`[KEYWORD_VAR, IDENTIFIER, EQUAL_SIGN, UNSIGNEDINT ]`

### Parser (syntax analysis)

Given some grammar which describes the formal rules of composition of tokens parses groups them into statements and
expressions. The result of a parser is *Abstract Syntax Tree*
(AST). This tree captures the grammatical structure of a program.

### Semantic routines (semantic analysis)

Next phase of compiler is not strictly defined. Semantic routines
travers AST and are capable of deducing expressions types
(type checking) or other things which result in further analysis
of the program. Output of such routines is usually *Intermediate
Representation* (IR) which is representation suitable for more
detailed analysis.

### Optimizer

Optimizer is responsible for transforming IR in a way that makes
it more efficient, more compact and more performant.


### Code Generator

Code generator consumes IR and generates corresponding assembly
instructions. It involves *register allocation*, *instruction
selection* and *sequencing* aka (instruction scheduler).

- instruction selection - maps IR into native instruction
- register allocation - maps virtual registers into real ones
- sequencing - reorders instruction (different instructions
    take different amount of time)



