# Scanning (lexical analysis)
Scanning is a process in which the the input text is converted into
sequence of tokens. For example in natural language a sentence could be
divided into words and each of those words would be a token.
In programming languages there would be many different tokens because
programming languages require more precision about what given token represents.
Token could represent identifier, number, language keyword, comments and many
more. Tokens must be strictly specified how they can be constructed.
For example identifier could be described as "starts with a letter followed by
zero or more letters or numbers or underscores".


Scanners are sometimes called recognizers in another literature.
Definition of a recognizer given in "Engineering a Compiler" is `recognizer
is a program that identifies specific words in a stream of characters`.
Type of a token (syntactic category) is a classification of a words according
to its usage.


One kind of scanner implementation is a *hand-made scanner*. Below is
an implementation of such scanner for a language consisting of three
categories of tokens i.e. identifiers, binary operator *not equal* and unary
operator *not*.
```python
c = get_char()
if c == '!':
    c = get_char()
    if c == '='
        return Token.NOT_EQUAL
    else:
        put_char_back()
        return Token.NOT
elif is_letter(c):
    while is_alphanumeric(get_char()):
        pass
    put_char_back()
    return Token.IDENTIFIER
else:
    raise UnrecognizedToken()
```

Hand written scanners can be very cumbersome to write especially for
complex languages. 

For formal definition of scanner *regular expressions* are used with
*finite automata* (finite automata as a processing tool for REs).
These tools allow for very precise formal definition and
implementation of scanners.


## Regular expressions

Introduced in 1950 by Stephen Kleene regular expression (REs) is a language
for expressing patterns.

```
A regular expression is a string which denotes L(s), a set of strings drawn
from an alphabet E. L(s) is known as a "language of s".

- If a belongs to E then a is a regular expressions and L(a) = {a}
- 'e' is a regular expression and L('e') contains only an empty string

Given regular expression s and t:
1. s|t is a RE such that L(s|t) = L(s) U L(t) (alternation aka or)
2. st is a concatenation of all strings of s followed by t (concatenation)
3. s* is s concatenated zero or more times (Kleen closure)
```

d(o|i)g regular expression would match both dog and dig
In order to simplify things few additional operators can be introduced
- ? says something can be optional (I(am)? would match both I and Iam)
- \+ says one or more ((ab)+ would match one or more ab)
- [a-z] indicates any character in a given range
- [^x] can be rewritten as E - x (everything in a language but x)

Regular expressions obey several algebraic properties:
- associativity a|(b|c) = (a|b)|c
- commutativity a|b = b|a
- distribution a(b|c) = ab|ac
- idempotency a\*\* = a\*

So now formal definition for an identifier would be:
Identifier is a sequence of capital letters and digits, but a digit cannot
be first.
[A-Z]+([A-Z]|[0-9])\*

## Finite Automata
*Finite Automata* is an abstract machine that can be used to represent
certain forms of computation. It happens that one of such forms are regular
expressions.

It can be represented as a set of states from which one is a starting state
and there is one or more accepting states.
![](../static/fa.png)  

In the image above accepting state is *q4* and starting state is *q0*.
So circles represent states, doubled circles accepting's states and edges
a transitions between those states. Transitions are made on symbols which are
drown from a language (*E*). Finite automaton *rejects* the input string if it
ends in a non-accepting state or if there is no edge corresponding to the
current symbol.

### Deterministic Finite Automata
*DFA* is a special case of *FA* in which there cannot be more than one outgoing
edges for a given symbol. This way there is no ambiguity (no need to know the
later part of the input string to decide which edge to choose). This makes
DFA much easier to implement in software.

### Non-deterministic Finite Automata
*NFA* is a valid finite automata but allows ambiguity (many transitions on same
symbol) and allows *empty string transitions* (e - epsilon).
Implementing NDA is possible but inefficient (need to keep track and look ahead
of many different possible choices).


## Conversion algorithms
So how to get from regular expressions to code?
We go step by step from one representation to another using conversion
algorithms.

- RE -> NFA is Thompson's Construction
- NFA -> DFA is Subset Construction
- DFA -> Code is Transition Matrix

### Thompson's Construction
Thompson's Construction uses prebuilt elements to build larger constructions.
We have constructions for alternation, concatenation and Kleene's closure and
we can use them to build bigger construction.
![](../static/thompsons.png)  


### Subset Construction
Subset construction allows us to build unambiguous and free of epsilon
transition deterministic finite automations.
![](../static/subset.png)  


Finite automatons cannot be used for everything. For example one cannot describe
a structure of a program using it (for example how to describe arbitrary number
of parenthesis nesting, automatons for 1000 nested parentheses would be huge
and thus inefficient). For that *parsers* are used.


## Scanner generators

Going from REs to code is a very long and hard road. That is why there are
already built solutions to that problem called scanner generators. The most 
popular is *Flex* or its older counterpart *Lex*. They can generate *C* code
which is capable of processing files and giving sequence of tokens in return.
What they need is just a definition of REs, token categories etc.
Example of Flex input file is:

```C
	   %{
	   /* need this for the call to atof() below */
	   #include <math.h>
	   %}

	   DIGIT    [0-9]
	   ID	    [a-z][a-z0-9]*

	   %%

	   {DIGIT}+    {
		       printf( "An integer: %s (%d)\n", yytext,
			       atoi( yytext ) );
		       }

	   {DIGIT}+"."{DIGIT}*	      {
		       printf( "A float: %s (%g)\n", yytext,
			       atof( yytext ) );
		       }

	   if|then|begin|end|procedure|function	       {
		       printf( "A keyword: %s\n", yytext );
		       }

	   {ID}	       printf( "An identifier: %s\n", yytext );

	   "+"|"-"|"*"|"/"   printf( "An operator: %s\n", yytext );

	   "{"[^}\n]*"}"     /* eat up one-line comments */

	   [ \t\n]+	     /* eat up whitespace */

	   .	       printf( "Unrecognized character: %s\n", yytext );

	   %%

	   main( argc, argv )
	   int argc;
	   char **argv;
	       {
	       ++argv, --argc;	/* skip over program name */
	       if ( argc > 0 )
		       yyin = fopen( argv[0], "r" );
	       else
		       yyin = stdin;

	       yylex();
	       }
```

In this example scanner performs some action
for each recognized token (prints it out).